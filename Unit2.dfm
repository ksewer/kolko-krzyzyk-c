object Form2: TForm2
  Left = 532
  Top = 266
  Width = 645
  Height = 397
  Caption = 'Form2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 160
    Top = 16
    Width = 247
    Height = 36
    Caption = 'Kto zaczyna gr'#281'?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -31
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 120
    Top = 160
    Width = 348
    Height = 36
    Caption = 'Jaka figura rozpoczyna?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -31
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Image1: TImage
    Left = 112
    Top = 216
    Width = 100
    Height = 100
  end
  object Image2: TImage
    Left = 360
    Top = 216
    Width = 100
    Height = 100
  end
  object Button1: TButton
    Left = 104
    Top = 80
    Width = 137
    Height = 41
    Caption = 'Gracz'
    TabOrder = 0
  end
  object Button2: TButton
    Left = 328
    Top = 80
    Width = 137
    Height = 41
    Caption = 'Komputer'
    TabOrder = 1
  end
end
