object Form1: TForm1
  Left = 303
  Top = 56
  Width = 948
  Height = 657
  Caption = 'KiK'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pole1: TImage
    Left = 152
    Top = 216
    Width = 100
    Height = 100
    OnClick = pole1Click
  end
  object pole2: TImage
    Left = 256
    Top = 216
    Width = 100
    Height = 100
    OnClick = pole2Click
  end
  object pole3: TImage
    Left = 360
    Top = 216
    Width = 100
    Height = 100
    OnClick = pole3Click
  end
  object pole4: TImage
    Left = 152
    Top = 320
    Width = 100
    Height = 100
    OnClick = pole4Click
  end
  object pole5: TImage
    Left = 256
    Top = 320
    Width = 100
    Height = 100
    OnClick = pole5Click
  end
  object pole6: TImage
    Left = 360
    Top = 320
    Width = 100
    Height = 100
    OnClick = pole6Click
  end
  object pole7: TImage
    Left = 152
    Top = 424
    Width = 100
    Height = 100
    OnClick = pole7Click
  end
  object pole8: TImage
    Left = 256
    Top = 424
    Width = 100
    Height = 100
    OnClick = pole8Click
  end
  object pole9: TImage
    Left = 360
    Top = 424
    Width = 100
    Height = 100
    OnClick = pole9Click
  end
  object pole_tura: TImage
    Left = 768
    Top = 256
    Width = 30
    Height = 30
  end
  object Label1: TLabel
    Left = 560
    Top = 256
    Width = 205
    Height = 37
    Caption = 'Tura gracza: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 528
    Top = 32
    Width = 237
    Height = 29
    Caption = 'Kto rozpoczyna gr'#281'?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -23
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object jakafigura1: TImage
    Left = 112
    Top = 72
    Width = 30
    Height = 30
    OnClick = jakafigura1Click
  end
  object jakafigura2: TImage
    Left = 328
    Top = 72
    Width = 30
    Height = 30
    OnClick = jakafigura2Click
  end
  object Label3: TLabel
    Left = 104
    Top = 32
    Width = 279
    Height = 29
    Caption = 'Jaka figura rozpoczyna?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -23
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button1: TButton
    Left = 560
    Top = 360
    Width = 217
    Height = 73
    Caption = 'Resetuj gr'#281
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -31
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = FormCreate
  end
  object Button2: TButton
    Left = 480
    Top = 72
    Width = 137
    Height = 33
    Caption = 'Gracz'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 648
    Top = 72
    Width = 137
    Height = 33
    Caption = 'Komputer'
    TabOrder = 2
    OnClick = Button3Click
  end
end
